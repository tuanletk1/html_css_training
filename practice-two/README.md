# Practice-two

## Overview

- This document provides the requirement for HTML CSS practice two
- Using HTML/CSS  to build a responsive website base on design.

- Design: [Hofmann UI Kit #1](https://www.figma.com/file/UUWUZGFTxDW3mWocVG8pzKhv/Hofmann-UI-Kit---Page-Templates)

## Technical

- HTML5 / CSS3
- Grid CSS


## Timeline

- 8 days(+2 days) ( 2022/07/4 – 2022/07/14 )

## Teamsize

- 1 dev

## Editor

- Visual Studio Code

## REQUIREMENTS

- Work fine on Chrome browser latest version
- Getting the code to work across the browser's latest versions (Chrome, MS Edge)

## Targets

- Apply knowledge to responsive practice one design
- Used media queries for popular screen size

## How to run the project

**Step one:** Clone the code folder from git to your device

- Choose a path to save that file -> At that path open the command window
- Run command
  >git clone https://gitlab.com/tuanletk1/html_css_training

**Step two:** Run project

- Turn on Visual Studio Code. Open the folder you just cloned to your computer.

- Run start project
  >start index.html
